<?php
/**
 * Class to represent address entity in the addressbook
 * @author Arfeen <arfeenster@gmail.com>
 */

class ClassAddress {

    /**
     *
     * @var string Street Address
     */
    public $streetaddress;

    /**
     *
     * @var string Phone Number
     */
    public $phonenumber;

    /**
     *
     * @var string emailaddress
     */
    public $emailaddress;

    /**
     * 
     * @param int $personid Person id for which address is being added
     * @throws Exception If person id is not provided
     * @return array DB recordset
     */
    public function addAddress($personid) {

        if (empty($personid)) {
            throw new Exception('Address must need a person id');
        }

        try {
            $db = new SQLite3Database("addressbook.db");
            $groupinfo = array("person_id" => $personid,
                "street_address" => $this->streetaddress,
                "phonenumber" => $this->phonenumber,
                "email_address" => $this->emailaddress
            );
            $db->insert("addresses", $groupinfo);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}
