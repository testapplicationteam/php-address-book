<?php

/**
 * 
 * Class to represent person methods and properties
 * @author Arfeen <arfeenster@gmail.com>
 */
class ClassPerson {

    /**
     *
     * @var string First Name
     */
    public $firstname;

    /**
     *
     * @var string Last Name
     */
    public $lastname;

    /**
     *
     * @var int Person ID
     */
    private $personid;

    /**
     * Function to add a person in the DB store
     * @return int Person id of the newly added person
     * 
     */
    public function addPerson() {
        try {
            $db = new SQLite3Database("addressbook.db");
            $personarray = array("firstName" => $this->firstname, "lastName" => $this->lastname);
            $this->personid = $db->insert("addressbook", $personarray);
            return $this->personid;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Function to find a person by email address
     * @param string $email Email address or substring of email
     * @return array DB recordset 
     */
    public function findPersonByEmail($email) {

        if (empty($email)) {
            throw new Exception('Email address should be provided');
        }

        $match = addcslashes($email, '%_');


        try {
            $db = new SQLite3Database("addressbook.db");
            $sql = $db->prepare("SELECT * FROM addresses WHERE email_address like %s ", "%" . $match . "%");
            $db->query($sql);
            return $db->fetch_row($db->results, true);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Function to attach a person to any group
     * @param string $groupid Group id of the group     
     */
    public function attachGroup($groupid) {
        try {
            $db = new SQLite3Database("addressbook.db");
            $groupinfo = array("group_id" => $groupid, "person_id" => $this->personid);
            $this->groupid = $db->insert("group_mapping", $groupinfo);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Function to fetch all groups of the person is attached to.
     * @param string $personid Person id
     * @return array DB recordset 
     */
    public function getAllGroupsByMember($personid) {

        try {
            $db = new SQLite3Database("addressbook.db");
            $sql = $db->prepare("SELECT * FROM group_mapping WHERE person_id = %d ", $personid);
            $db->query($sql);
            return $db->fetch_row($db->results, true);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Function to fetch persons by first name or last name or both
     * @param string $firstname first name
     * @param string $firstname last name
     * @return array DB recordset 
     */
    public function getPersonsByName($firstname = '', $lastname = '') {

        if (empty($firstname) && empty($lastname)) {
            throw new Exception('At least one name should be provided');
        }

        try {
            $db = new SQLite3Database("addressbook.db");
            $sql = $db->prepare("SELECT * FROM addressbook WHERE (firstname = %s OR lastname = %s) ", $firstname, $lastname);
            $db->query($sql);
            return $db->fetch_row($db->results, true);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}
