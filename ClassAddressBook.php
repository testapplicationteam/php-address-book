<?php

/**
 * 
 * Class to represent entire addressbook main operations
 * @author Arfeen <arfeenster@gmail.com>
 */
class ClassAddressBook {

    /**
     * Constructor     
     */
    public function __construct() {
        $this->person = new ClassPerson();
    }

    /**
     * 
     * @param string $grouptitle title of the group
     * @return int group id of the newly created group
     * @throws Exception Title should not be empty
     * 
     * 
     */
    public function createGroup($grouptitle) {
        if (empty($grouptitle)) {
            throw new Exception('Group must need a title');
        }

        try {
            $db = new SQLite3Database("addressbook.db");
            $groupinfo = array("group_title" => $grouptitle);
            return $db->insert("groups", $groupinfo);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * 
     * @param type $groupid Group to be find
     * @return array DB recordser
     */
    public function getAllMembersInGroup($groupid) {

        try {
            $db = new SQLite3Database("addressbook.db");
            $sql = $db->prepare("SELECT * FROM group_mapping WHERE group_id = %d ", $groupid);
            $db->query($sql);
            return $db->fetch_row($db->results, true);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}
