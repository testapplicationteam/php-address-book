<?php

use PHPUnit\Framework\TestCase;

require 'autoload.php';

class AddressBookTest extends TestCase {

    /**
     * Test for adding new person in the addressbook
     */
    public function testAddPerson() {

        $person = new ClassPerson();
        $this->assertGreaterThan(0, $person->addPerson());
    }

    /**
     * Test for adding group in the addressbook
     */
    public function testAddGroup() {

        $addressboook = new ClassAddressBook();
        $groupid = $addressboook->createGroup("Collegues Group");
        $this->assertGreaterThan(0, $groupid);
    }

    /**
     * Test for fetchnig members of the group
     */
    public function testGetAllMembersByGroupId() {

        $addressboook = new ClassAddressBook();
        $recordset = $addressboook->getAllMembersInGroup(2);
        $this->assertGreaterThan(0, $recordset);
    }

    /**
     * Test for fetching groups by member id
     */
    public function testGetAllGroupsByMemberId() {

        $person = new ClassPerson();
        $recordset = $person->getAllGroupsByMember(29);
        $this->assertGreaterThan(0, $recordset);
    }

    /**
     * Test for finding members by names
     */
    public function testFindMemberByNames() {

        $person = new ClassPerson();
        $recordset = $person->getPersonsByName("Muhammad");
        $this->assertGreaterThan(0, $recordset);

        $recordset = $person->getPersonsByName("", "Arfeen");
        $this->assertGreaterThan(0, $recordset);
    }

    /**
     * Test for finding member by email
     * 
     */
    public function testFindMemberByEmail() {

        $person = new ClassPerson();
        $recordset = $person->findPersonByEmail("ema");
        $this->assertGreaterThan(0, $recordset);
    }

}
