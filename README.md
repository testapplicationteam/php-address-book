#
#	ReadMe for PHP Address Book Test
#

#########################################################################################################
#### Note: This library is using an external library for DB operations. A SQlite DB is being used here. #
#### SQLite3Database.php is the external library.                                                       #
#########################################################################################################

TEST:

All the tests are included in a single file in tests/AddressBookTest.php

addressbook.db is the SQlite DB file which should be reside in the same directory where all the files exist.

###########################################################################################################


### API Documentation ###

Address book library consists of 4 entities:

Address Book
Person
Addresses
Groups

Features:

Add a person to address book
============================

ClassPerson can instantiated to achieve the activity of adding a new person.
firstName and lastName are the properties to be set. addPerson methos can be used to add the person in the DB.
A newly created person id will be returned.

    All following activities can be achived by using same above ClassPerson class.

    Find person by email 
    ====================
    The above class provides a method findPersonByEmail(email) which can be used to seek the person by given email address.

    Attach a group to person
    ========================
    A person can be attached in a group. attachGroup(group id) method can be used to achive this operation.


    Get the groups of person
    ========================
    Activity to find all attached groups of a person can be fetch by using getAllGroupsByMember(person id).



Create a new group
==================
A group can be created by using createGroup method in the ClassAddressBook.


Fetch all members of the group
==============================
getAllMembersInGroup(group id) method can be used from ClassAddressBook to achive all the members of a particular group.


Add Address
===========
Adding an address is easy by using addAddress function of ClassAddress.



All of the APIs can be consumed separately, as well as they can be used in one address book setup as show in example in "application.php".


### Design-only question ####

Answer:

Find a value using sub string of the provided input in DB should be done by given SQL functions or clauses, example, LIKE. This is a direct usage. More alike functions or operators can be used to achieve the goal. If its not about the DB, then definitely we have a list maintained somewhere in the memory in the form of array. If this is the case, then definitely we will have to traverse the array for each of the element to search for a given string or the pattern.