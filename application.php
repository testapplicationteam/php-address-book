<?php


/**
 * Example application which is consuming addressbook library
 */

require 'autoload.php';
/**
 * Address book setup
 */
$addressboook = new ClassAddressBook();


/**
 * Adding a person with necessary details
 */
$addressboook->person->firstname = "Muhammad";
$addressboook->person->lastname = "Arfeen";
$personid = $addressboook->person->addPerson();

/**
 * Creating addresses and attaching with the person record
 */
$address = new ClassAddress();
$address->phonenumber = "100100100";
$address->streetaddress = "Address 1";
$address->emailaddress = "email1@email.com";
$address->addAddress($personid);

$address = new ClassAddress();
$address->phonenumber = "100100101";
$address->streetaddress = "Address 2";
$address->emailaddress = "email2@email.com";
$address->addAddress($personid);


/**
 * Create a group in address book
 */
$groupid = $addressboook->createGroup("Collegues Group");
$addressboook->person->attachGroup($groupid);

$groupid = $addressboook->createGroup("Family Group");
$addressboook->person->attachGroup($groupid);
?>
